package ie.com.money.MoneyProject.controller;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ie.com.money.MoneyProject.model.Gasto;
import ie.com.money.MoneyProject.model.Greeting;

@RestController
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    }
    
    @RequestMapping("/inserirGasto")
    public Gasto inserirGastos(@RequestParam(value="description",required=true) String description, 
    							  @RequestParam(value="value",required=true) String value,
    							  @RequestParam(value="date",required=true) String date) {
        return new Gasto(description, value, date);
    }
    
    

}
