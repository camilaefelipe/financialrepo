package ie.com.money.MoneyProject.repository;

import org.springframework.data.repository.CrudRepository;

import ie.com.money.MoneyProject.entity.User;

public interface UserRepository extends CrudRepository<User, Long> {

}
