package ie.com.money.MoneyProject.model;

public class Gasto {
	
	private String description;
	private String date;
	private String value;
		
	public Gasto(String description, String date, String value) {
		super();
		this.description = description;
		this.date = date;
		this.value = value;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

}
